# Created by Ansari at 03/13/2019
import matplotlib.pyplot as plt
import pandas as pd

names = [
    "Nearest Neighbors", "AdaBoost", "XGBoost", "Gradient Boost",
    "Decision Tree", "Random Forest", "ExtraTreesClassifier"
]

accuracy = [0.7267, 0.7093, 0.7439, 0.7289, 0.7069, 0.7146, 0.7052]

s = pd.Series(accuracy, index=names)

#Set Description
plt.title("Classifiers Score on Duplicate Question Pair dataset")
plt.ylim(0.65,0.75)
plt.xlabel('Classifiers')
plt.ylabel('Accuracy')
plt.xticks(rotation=30, horizontalalignment='right')

#Set tick colors:
ax = plt.gca()
ax.tick_params(axis='x', colors='blue')
ax.tick_params(axis='y', colors='red')


#Plot the data
my_colors = ['r','g','b','k','y','m','c']
s.plot(kind='bar', color=my_colors)
plt.xticks(rotation=30, horizontalalignment='right')
plt.savefig('classifier_acc.png', bbox_inches='tight')

#plt.savefig('classifier_accuracy.png', bbox_inches='tight')
plt.show()
