# Created by Ansari at 02/27/2019
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, ExtraTreesClassifier, \
    GradientBoostingClassifier
from sklearn import preprocessing
import pandas as pd
import numpy as np
import xgboost as xgb

names = [
    "Nearest Neighbors", "AdaBoost", "XGBoost", "Gradient Boost",
    "Decision Tree", "Random Forest", "ExtraTreesClassifier"
]

classifiers_list = [
    KNeighborsClassifier (10),
    AdaBoostClassifier (algorithm='SAMME.R', base_estimator=None, learning_rate=0.8, n_estimators=50),
    xgb.XGBClassifier (learning_rate=0.1, max_depth=5, n_estimators=100),
    GradientBoostingClassifier (learning_rate=0.1, n_estimators=100, max_depth=3, min_samples_split=2,
                                min_samples_leaf=1, subsample=1, max_features='sqrt', random_state=10),
    DecisionTreeClassifier (max_depth=5),
    RandomForestClassifier (max_depth=5, n_estimators=100, max_features=7),
    ExtraTreesClassifier (max_depth=5, n_estimators=100, max_features=7),
]

#Read csv feature dataset
dframe = pd.read_csv ('./data/quora_features_set.csv')


def clean_dataset(df):
    assert isinstance (df, pd.DataFrame), "Create panda dataframe"
    df.dropna (inplace=True)
    indices_to_keep = ~df.isin ([np.nan, np.inf, -np.inf]).any (1)
    return df[indices_to_keep].astype (np.float64)


Z = dframe.drop (['question1', 'question2'], 1)
print (Z.head (10))
Z = clean_dataset (Z)
y = Z['is_duplicate']
X = Z.drop (['is_duplicate'], 1)

min_max_scaler = preprocessing.MinMaxScaler ()
X = min_max_scaler.fit_transform (X)
X_train, X_test, y_train, y_test = train_test_split (X, y, test_size=0.35, stratify=Z.is_duplicate, random_state=42)
feat_test = DecisionTreeClassifier (min_samples_split=0.1)
feat_test.fit (X_train, y_train)
print ("\n\nImportant features:\n", feat_test.feature_importances_)

for name, clsf in zip (names, classifiers_list):
    clsf.fit (X_train, y_train)
    score = clsf.score (X_test, y_test)
    print (name + ' Accuracy Score:{:.4f}'.format (score))
